
import cv2
import time
import matplotlib.pyplot as plt
import json
import moviepy.editor
import cv2

# import darknet functions to perform object detections
#from darknet import *
# load in our YOLOv4 architecture network
from darknet import *
network, class_names, class_colors = load_network("cfg/yolov4-obj.cfg", "data/obj.data", "data/yolov4-obj_last.weights")
width = network_width(network)
height = network_height(network)
queue=[]
wt=[]
wy1=[]
wy2=[]
wy3=[]
# darknet helper function to run detection on image
def darknet_helper(img, width, height):
  darknet_image = make_image(width, height, 3)
  img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
  img_resized = cv2.resize(img_rgb, (width, height),
                              interpolation=cv2.INTER_LINEAR)

  # get image ratios to convert bounding boxes to proper size
  img_height, img_width, _ = img.shape
  width_ratio = img_width/width
  height_ratio = img_height/height

  # run model on darknet style image to get detections
  copy_image_from_bytes(darknet_image, img_resized.tobytes())
  detections = detect_image(network, class_names, darknet_image)
  free_image(darknet_image)
  return detections, width_ratio, height_ratio

def bb_intersection_over_union(boxA, boxB):
     xA = max(boxA[0], boxB[0])
     yA = max(boxA[1], boxB[1])
     xB = min(boxA[2], boxB[2])
     yB = min(boxA[3], boxB[3])
     interArea = max(0, xB - xA + 1) * max(0, yA - yB + 1)
     boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[1] - boxB[3] + 1)
     if interArea==0:
         return 0

     return interArea/boxBArea


def detect_violations(inp_path,out_path):
      #path='/content/gdrive/MyDrive/My Drive/yolov4/video.mp4'
      path=inp_path
      video = cv2.VideoCapture(path)

      #defing trackers to track violations for each workspace
      ucv_allocated=[0,0,0]
      no_of_frames_ucv=[0,0,0]
      ocv_allocated=[0,0,0]
      no_of_frames_ocv=[0,0,0]

      if (video.isOpened() == False): 
         print("Error reading video file")
      frame_width = int(video.get(3))
      frame_height = int(video.get(4))
      size = (frame_width, frame_height)
      #res_path='/content/gdrive/MyDrive/My Drive/yolov4/operator.avi'
      res_path=out_path
      
      tot_frames=0
      while True:
        ret,image=video.read()
        if ret!=True:
          break
        else:
          tot_frames+=1
      myvideo=moviepy.editor.VideoFileClip(path)
      tt=int(myvideo.duration)
      fps=int(((tot_frames)//tt)+1)
      print(fps ,tot_frames,tt)

      result = cv2.VideoWriter(res_path, cv2.VideoWriter_fourcc(*'MJPG'),fps, size)
      #result = cv2.VideoWriter(res_path, cv2.VideoWriter_fourcc(*'H264'), fps, size)
      video = cv2.VideoCapture(path)

      count=0
      w1=[]
      w2=[]
      w3=[]
      st=time.time()
      red=[0,0,255]
      green=[0,255,0]
      #fps=10
      while(True):
        ret, image = video.read()
        if ret == True: 
          #finding detections in the video
          detections, width_ratio, height_ratio = darknet_helper(image, width, height)

          #here workspace list is defined to keep track of no of oerators present in each workspace in each frame
          workspace=[0,0,0]

          
          print("frame",count)

          #for representing time in each frame
          cv2.putText(image, "Time:{0} ".format(count*(1/fps)),(200,250), cv2.FONT_HERSHEY_SIMPLEX, 1,red,2, 2)
          
          #loop through each detections
          for label, confidence, bbox in detections:
             left, top, right, bottom = bbox2points(bbox)
             left, top, right, bottom = int(left * width_ratio), int(top * height_ratio), int(right * width_ratio), int(bottom * height_ratio)
             #for all workspace find dectected operator intersection area with respect to each and every workspace i.e 3 in this case
             iou1=bb_intersection_over_union([796,333,1080,760],[left,bottom,right,top])
             iou2=bb_intersection_over_union([937,126,1129,450],[left,bottom,right,top])
             iou3=bb_intersection_over_union([1156,248,1330,750],[left,bottom,right,top])
             
             #Now find the workspace, that the detected object belongs to by comparing its intersection area,i.e workspace to which it share maximum intersection area
             if iou1>iou2 and iou1>iou3 and iou1>=0.7:
                      workspace[0]+=1
             #if bothiou1 and iou2 are equal then we need to find distance between centroid of the operator and the centroid of the workspace,then we are going to add it to the workspace to which distance is less
             elif iou1>=iou2 and iou1>=iou3 and iou1>=0.7:
               wsp1x=(796+1080)/2
               wsp1y=(760+333)/2
               wsp2x=(937+1129)/2
               wsp2y=(450+126)/2
               px=(left+right)/2
               py=(top+bottom)/2
               dis1=(wsp1x-px)*(wsp1x-px)+(wsp1y-py)*(wsp1y-py)
               dis2=(wsp2x-px)*(wsp2x-px)+(wsp2y-py)*(wsp2y-py)
               if dis1>dis2:
                  workspace[1]+=1
               else:
                  workspace[0]+=1
                  
             elif iou2>=iou3 and iou2>=iou1 and iou2>=0.7:
               workspace[1]+=1
             elif iou3>=0.7 and iou3>=iou2 and iou3>=iou1:
                  workspace[2]+=1
             else:
                pass
             
             cv2.rectangle(image, (left, top), (right, bottom), class_colors[label], 2)
             cv2.putText(image, "{} [{:.2f}]".format(label, float(confidence)),(left, top - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5,class_colors[label], 2)
        
          #now based on the allocations we need to manipulate the boxes
          #represents violations
          #if it is overcrowded at workspace1
          if workspace[0]>1:
             if ocv_allocated[0]==1:
                  no_of_frames_ocv[0]+=1
                  if no_of_frames_ocv[0]==fps:
                      w1.append({})
                     
                      w1[-1]["start_time"]=(count)*(1/fps) 
                      w1[-1]["Overdue"]=1
                      w1[-1]["underdue"]=0
                      w1[-1]["end_time"]="not ended"
                  if no_of_frames_ocv[0]>=fps:
                      wy1.append(workspace[0])
                      cv2.rectangle(image, (796, 760), (1080, 333), red, 2)
                  else:
                      wy1.append(1)
                      cv2.rectangle(image, (796, 760), (1080, 333), green, 2)
             else:
                  ocv_allocated[0]=1
                  no_of_frames_ocv[0]=1
                  wy1.append(1)
                  cv2.rectangle(image, (796, 760), (1080, 333), green, 2)


          #under crowded  violation at workspace 1
          elif workspace[0]==0:
              if ucv_allocated[0]==1:
                     no_of_frames_ucv[0]+=1
                     if no_of_frames_ucv[0]==fps:
                        w1.append({})
                        w1[-1]["start_time"]=(count)*(1/fps) 
                        #w1[-1]["start_time"]=time.time()-st
                        w1[-1]["Overdue"]=0
                        w1[-1]["underdue"]=1
                        w1[-1]["end_time"]="not ended"

                     if no_of_frames_ucv[0]>=fps:
                         wy1.append(workspace[0])
                         cv2.rectangle(image, (796, 760), (1080, 333), red, 2)
                     else:
                         wy1.append(1)
                         cv2.rectangle(image, (796, 760), (1080, 333), green, 2)
              else:
                    ucv_allocated[0]=1
                    no_of_frames_ucv[0]=1
                    wy1.append(1)
                    cv2.rectangle(image, (796, 760), (1080, 333),green, 2)

          #not a violation
          else:
               if ucv_allocated[0]==1:
                   
                    if no_of_frames_ucv[0]>=fps:
                        w1[-1]["end_time"]=(count)*(1/fps) 
                        #w1[-1]["end_time"]=time.time()-st
                    ucv_allocated[0]=0
                    no_of_frames_ucv[0]=0
               if ocv_allocated[0]==1:
                   
                   if no_of_frames_ocv[0]>=fps:
                        w1[-1]["end_time"]=(count)*(1/fps) 
                        #w1[-1]["end_time"]=time.time()-st
                   ocv_allocated[0]=0
                   no_of_frames_ocv[0]=0
               wy1.append(1)
               cv2.rectangle(image, (796, 760), (1080, 333), green, 2)

          #if over crowded violation occurs in workspace area 2
          if workspace[1]>1:
              if ocv_allocated[1]==1:
                  no_of_frames_ocv[1]+=1
                  if no_of_frames_ocv[1]==fps:
                    w2.append({})
                    w2[-1]["start_time"]=(count)*(1/fps)
                    #w2[-1]["start_time"]=time.time()-st
                    w2[-1]["Overdue"]=1
                    w2[-1]["underdue"]=0
                    w2[-1]["end_time"]="not ended"

                  if no_of_frames_ocv[1]>=fps:
                      wy2.append(workspace[1])
                      cv2.rectangle(image, (937, 459), (1129, 126), red, 2)
                  else:
                      wy2.append(1)
                      cv2.rectangle(image, (937, 459), (1129, 126), green, 2)
              else:
                  ocv_allocated[1]=1
                  no_of_frames_ocv[1]=1
                  wy2.append(1)
                  cv2.rectangle(image, (937, 459), (1129, 126), green,2)
          #undercrowded at workspace 2
          elif workspace[1]==0:
                 if ucv_allocated[1]==1:
                      no_of_frames_ucv[1]+=1
                      if no_of_frames_ucv[1]==fps:
                        w2.append({})
                        w2[-1]["start_time"]=(count)*(1/fps)
                       # w2[-1]["start_time"]=time.time()-st
                        w2[-1]["Overdue"]=0
                        w2[-1]["underdue"]=1
                        w2[-1]["end_time"]="Not ended"

                      if no_of_frames_ucv[1]>=fps:
                            wy2.append(workspace[1])
                            cv2.rectangle(image, (937, 459), (1129, 126), red, 2)
                      else:
                          wy2.append(1)
                          cv2.rectangle(image, (937, 459), (1129, 126), green, 2)
                             

                 else:
                     ucv_allocated[1]=1
                     no_of_frames_ucv[1]=1
                     wy2.append(1)
                     cv2.rectangle(image, (937, 459), (1129, 126), green, 2)
          #No violation in workspace 2
          else:
            if ucv_allocated[1]==1:
               if no_of_frames_ucv[0]>=fps:
                    w2[-1]["end_time"]=(count)*(1/fps)
                    #w2[-1]["end_time"]=time.time()-st
              
               no_of_frames_ucv[1]=0
               ucv_allocated[1]=0
            if ocv_allocated[1]==1:
              if no_of_frames_ocv[1]>=fps:
                  w2[-1]["end_time"]=(count)*(1/fps)
                  #w2[-1]["end_time"]=time.time()-st
              no_of_frames_ocv[1]=0
              ocv_allocated[1]=0
            wy2.append(1)
            cv2.rectangle(image, (937, 459), (1129, 126), green, 2)

          #if overcrowded in workspace 3
          if workspace[2]>1:
             if ocv_allocated[2]==1:
                 no_of_frames_ocv[2]+=1
                 if no_of_frames_ocv[2]==fps:
                   w3.append({})
                   #myvideo=moviepy.editor.VideoFileClip(res_path)
                   #w3[-1]["start_time"]=(int(myvideo.duration))
                   w3[-1]["start_time"]=(count)*(1/fps)
                  # w3[-1]["start_time"]=time.time()-st
                   w3[-1]["Overdue"]=1
                   w3[-1]["underdue"]=0
                   w3[-1]["end_time"]="not ended"

                 if no_of_frames_ocv[2]>=fps:
                     wy3.append(workspace[2])
                     cv2.rectangle(image, (1156, 750), (1330, 248), red, 2)
                 else:
                     wy3.append(1)
                     cv2.rectangle(image, (1156, 750), (1330, 248), green, 2)
             else:
                  ocv_allocated[2]=1
                  no_of_frames_ocv[2]=1
                  wy3.append(1)
                  cv2.rectangle(image, (1156, 750), (1330, 248), green, 2)
          #if undercrowded in workspace3 
          elif workspace[2]==0:
              if ucv_allocated[2]==1:
                 no_of_frames_ucv[2]+=1
                 if no_of_frames_ucv[2]==fps:
                   w3.append({})
                   w3[-1]["start_time"]=(count)*(1/fps)
                   w3[-1]["Overdue"]=0
                   w3[-1]["underdue"]=1
                   w3[-1]["end_time"]="not ended"

                 if no_of_frames_ucv[2]>=fps:
                      wy3.append(workspace[2])
                      cv2.rectangle(image, (1156, 750), (1330, 248), red, 2)
                 else:
                      wy3.append(1)
                
                      cv2.rectangle(image, (1156, 750), (1330, 248), green, 2)
              else:
                   ucv_allocated[2]=1
                   no_of_frames_ucv[2]=1
                   wy3.append(1)
                   cv2.rectangle(image, (1156, 750), (1330, 248), green, 2)
          #No violation in workspace 3
          else:
            if ucv_allocated[2]==1:
              
              if no_of_frames_ucv[2]>=fps:
                  w3[-1]["end_time"]=(count)*(1/fps)
              ucv_allocated[2]=0
              no_of_frames_ucv[2]=0
            if ocv_allocated[2]==1:
              
              if no_of_frames_ocv[2]>=fps:
                   w3[-1]["end_time"]=(count)*(1/fps)
              ocv_allocated[2]=0
              no_of_frames_ocv[2]=0
            wy3.append(1)
            cv2.rectangle(image, (1156, 750), (1330, 248), green, 2)
          result.write(image)
          wt.append(count*(1/fps))
          
          count+=1
          frame = cv2.imencode('.jpeg', image,[cv2.IMWRITE_JPEG_QUALITY,40])[1].tobytes()
          queue.append(frame)

        else:
         break
      
      #preparing violations dictionaried for each workspace
      res={"workspace1_violations":{},"workspace2_violations":{},"workspace3_violations":{}}
      for i in range(len(w1)):
        
        res["workspace1_violations"][i]=w1[i]
      for i in range(len(w2)):
        
        res["workspace2_violations"][i]=w2[i]
      for i in range(len(w3)):
        
        res["workspace3_violations"][i]=w3[i]

     



      return res
     
    

