from flask import Flask,request,Response,jsonify,make_response,render_template,redirect,url_for,Response
import numpy as np
import os
from base64 import b64encode
import cv2
import time
import matplotlib.pyplot as plt
import json
import moviepy.editor
import model
from flask_ngrok import run_with_ngrok
import os
from time import sleep
from flask import copy_current_request_context
import threading
import datetime


#Application configuration details

UPLOAD_FOLDER = 'static/uploads/'
app = Flask(__name__)
app.secret_key = "secret"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0






#This function takes video file and process it and generates analytics as a response
run_with_ngrok(app)
@app.route('/process')
def process():
      print("Processing your video.....")
      base_path=os.path.join(app.config['UPLOAD_FOLDER'], 'video.mp4')

      inp_path=base_path
      out_path=os.path.join(app.config['UPLOAD_FOLDER'],'operator.avi')
      
      #find violations in the video
      res=model.detect_violations(inp_path,out_path)

      #finding frequency of violations 
      vio_w1=len(list(res["workspace1_violations"].keys()))
      vio_w2=len(list(res["workspace2_violations"].keys()))
      vio_w3=len(list(res["workspace3_violations"].keys()))

      #segmenting video to chunks (i.e .ts format files)
     
      os.popen('ffmpeg -i static/uploads/operator.avi -hls_time 1  -hls_playlist_type vod -hls_segment_filename "static/uploads/video_segments_%0d.ts" static/uploads/playlist.m3u8')
      #plotting
      plt.subplot(3,1,1)
      plt.plot(model.wt,model.wy1)
      plt.title("WORKAREA ANALYTICS")
      plt.ylabel("N.O.V1")
      plt.xlabel("t")

      plt.subplot(3,1,2)
      plt.plot(model.wt,model.wy2)
      
      plt.ylabel("N.O.V2")
      plt.xlabel("t")

      plt.subplot(3,1,3)
      plt.plot(model.wt,model.wy3)
      
      plt.ylabel("N.O.V3")
      plt.xlabel("t")
      plt.subplot_tool()
      filename="image.png"
      plt.savefig(os.path.join(app.config['UPLOAD_FOLDER'], filename))
      filename=url_for('static',filename='uploads/'+filename)

      #video coverting from avi to mp4
     # time.sleep(2)
     
     # os.popen('ffmpeg -i static/uploads/operator.avi -hls_time 1  -hls_playlist_type vod -hls_segment_filename "static/uploads/video_segments_%0d.ts" static/uploads/playlist.m3u8')

      
      return render_template('upload.html', filename=filename,res=res)


#uploading video file to static/uploads folder
@app.route('/upload', methods=['POST','GET'])
def upload():
    @copy_current_request_context
    def save_file(closeAfterWrite):
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " i am doing")
        f = request.files['file']
        f.filename='video.mp4'
        base_path=os.path.join(app.config['UPLOAD_FOLDER'], f.filename)
        f.save(base_path)
        closeAfterWrite()
        print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " write done")
        
    def passExit():
        pass
    if request.method == 'POST':
        f= request.files['file']
        normalExit = f.stream.close
        f.stream.close = passExit
        t = threading.Thread(target=save_file,args=(normalExit,))
        t.start()
        l=[t]
        for td in l:
          td.join()
        return redirect(url_for('process'))
    return render_template('index.html')

@app.route('/test')
def test():
  return render_template('test.html')

     

app.run()
